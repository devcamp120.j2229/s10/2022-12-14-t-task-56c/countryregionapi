package com.devcamp.api.countryregionapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.countryregionapi.model.Country;
import com.devcamp.api.countryregionapi.service.CountryService;

@CrossOrigin
@RestController
public class CountryController {
    @Autowired
    CountryService countryService;

    @GetMapping("/countries")
    public ArrayList<Country> getAllCountries() {
        //CountryService countryService = new CountryService();
        return countryService.getAllCountries();
    }

    @GetMapping("/country-info")
    public Country getCountryByCountryCode(@RequestParam String code) {
        return countryService.findCountryByCountryCode(code);
    }
}
