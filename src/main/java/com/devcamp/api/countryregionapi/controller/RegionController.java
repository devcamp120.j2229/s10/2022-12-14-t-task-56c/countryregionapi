package com.devcamp.api.countryregionapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.api.countryregionapi.model.Region;
import com.devcamp.api.countryregionapi.service.RegionService;

@CrossOrigin
@RestController
public class RegionController {    
    @Autowired
    RegionService regionService;

    @GetMapping("/region-info")
    public Region findRegionByRegionCode(@RequestParam(value="regionCode") String code) {
        return regionService.findRegionByRegionCode(code);
    }
}
