package com.devcamp.api.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.api.countryregionapi.model.Region;

@Service
public class RegionService {
    public ArrayList<Region> getVietnameseRegions() {
        ArrayList<Region> listRegions = new ArrayList<Region>();
        listRegions.add(new Region("HN", "Hà Nội"));
        listRegions.add(new Region("DN", "Đà Nẵng"));
        listRegions.add(new Region("HCM", "Hồ Chí Minh"));

        return listRegions;
    }

    public ArrayList<Region> getChineseRegions() {
        ArrayList<Region> listRegions = new ArrayList<Region>();
        listRegions.add(new Region("BK", "Bắc Kinh"));
        listRegions.add(new Region("NK", "Nam Kinh"));
        listRegions.add(new Region("QC", "Quảng Châu"));

        return listRegions;
    }
    
    public ArrayList<Region> getFrenchRegions() {
        ArrayList<Region> listRegions = new ArrayList<Region>();
        listRegions.add(new Region("PR", "Paris"));
        listRegions.add(new Region("LO", "Lyon"));
        listRegions.add(new Region("NC", "Nancy"));

        return listRegions;
    }    

    public Region findRegionByRegionCode(String regionCode) {
        ArrayList<Region> listRegions = new ArrayList<>();
        listRegions.addAll(getVietnameseRegions());
        listRegions.addAll(getChineseRegions());
        listRegions.addAll(getFrenchRegions());

        Region result = new Region();
        for (Region region : listRegions) {
            if (region.getRegionCode().equalsIgnoreCase(regionCode)) {
                result = region;
                break;
            }
        }

        return result;
    }
}
