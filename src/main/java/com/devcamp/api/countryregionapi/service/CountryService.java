package com.devcamp.api.countryregionapi.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.api.countryregionapi.model.Country;

@Service
public class CountryService {
    @Autowired
    RegionService regionService;
    public ArrayList<Country> getAllCountries() {
        //RegionService regionService = new RegionService();
        ArrayList<Country> listCountries = new ArrayList<Country>();
        Country vietnam = new Country("vn", "Việt Nam");
        vietnam.setRegions(regionService.getVietnameseRegions());
        listCountries.add(vietnam);

        Country china = new Country("cn","Trung Quốc", regionService.getChineseRegions());
        listCountries.add(china);

        Country franc = new Country();
        franc.setCountryCode("fr");
        franc.setCountryName("Pháp");
        franc.setRegions(regionService.getFrenchRegions());
        listCountries.add(franc);

        return listCountries;
    }

    public Country findCountryByCountryCode(String countryCode) {
        ArrayList<Country> listCountries = getAllCountries();
        Country result = new Country();
        for (Country country : listCountries) {
            if (country.getCountryCode().equalsIgnoreCase(countryCode)) {
                result = country;
                break;
            }
        }

        return result;
    }
}
